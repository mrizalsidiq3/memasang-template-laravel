<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function table()
    {
        return view('admin.table');
    }
    public function data_table()
    {
        return view('admin.data_table');
    }
}
