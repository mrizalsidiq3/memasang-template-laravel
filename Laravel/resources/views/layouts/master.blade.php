@include('partials.header')


<body class="hold-transition sidebar-mini layout-fixed">
  
  @include('partials.navbar')
          

          <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
     
      

          <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

            @yield('content')


            </div>
          </div>



</div>
</div>
</div>

@include('partials.footer')

  
