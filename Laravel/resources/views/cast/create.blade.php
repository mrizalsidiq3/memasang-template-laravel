
@extends('layouts.master');


@section('content')

  <div class="card">
    <div class="card-body">
    <form action="/cast" method="POST">
      @csrf
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Nama  </label>
    <div class="col-sm-10">
      <input type="text"  name="nama" class="form-control form-label" >
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
  </div>
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Umur  </label>
    <div class="col-sm-10">
      <input type="number" name="umur"  class="form-control form-label" placeholder=" .... years Old">
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
  </div>
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Bio  </label>
    <div class="col-sm-10">
  <textarea name="bio" class="form-control"></textarea>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  </div>
  </div>
 
  <button type="submit" class="btn btn-primary float-right">Submit</button>
</form>
    </div>
  </div>

@endsection