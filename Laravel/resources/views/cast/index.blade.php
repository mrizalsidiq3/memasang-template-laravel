@extends('layouts.master')
@section('content')

<div class="card">
    <div class="card-header">
        <a href="/cast/create" class="btn btn-primary">Tambah Data</a>
        <div class="card-body">
<table class="table table-striped">
  <thead>
      <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Aksi</th>
      </tr>
</thead>
<tbody>
        @forelse ($cast as $key => $val)
        <tr>
            <td> {{$key + 1 }} </td>
            <td> {{$val->nama}}</td>
            <td> {{$val->umur}}</td>
            <td> {{$val->bio}}</td>
            <td>
                <form action="/cast/{{$val->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{ $val->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{ $val->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete" onclick="return confirm('apakah anda yakin')">
                </form>
            </td>
        </tr>

        @empty
            <h1>No Data</h1>
        @endforelse
</tbody>
</table>
        </div>
    </div>
</div>

@endsection