<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TableController@dashboard');
Route::get('/table', 'TableController@table');
Route::get('/data_table', 'TableController@data_table');

// CRUD CAST
Route::get('/cast', 'CastController@index');

Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');
// Detail Data
Route::get('/cast/{cast_id}', 'CastController@show');
// Edit Data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// update
Route::put('/cast/{cast_id}', 'CastController@update');
// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');



